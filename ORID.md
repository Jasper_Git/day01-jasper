## Objective

- In the morning, I learned about the time arrangement of the AFS. in the next month, and realized the importance of autonomous learning through short films and through the Specific Plan, DO, Check and Adjust to implement independent learning.

- In the afternoon, I learned the concept diagrams used to organize and contact knowledge points, and the composition elements of the concept diagram, and how to complete the concept diagram around Focus Question. In addition, the group operates the concept diagram in the group. In addition, I also learned about Stand-Up Meeting's behavioral specifications. It can truly consolidate our knowledge, reduce doubts, and determine our learning direction.

## Reflective

- In the future process, I involve some technologies I have never touched, and I feel a little anxious.
- In the group concept map operation, I am very happy to cooperate with the team members for the first time and complete the task.
- An general, tomorrow is unknown. It is important to grasp today.

## Interpretive

- PDCA is from a specific plan, subsequently enforce, verification, adjustment, and iterates. Its form is similar to agile development.
- Mind map is to present the knowledge we know, and it is a tool tool. Different from the thinking map, the concept map is a process tool that connects the new knowledge points we explore.

## Decision

- In advance to preview unprepared technologies, and then keep up with the schedule and avoid hasty knowledge bombing.
- And before the start of Stand-Up Meeting, prepare the issues I want to say.

